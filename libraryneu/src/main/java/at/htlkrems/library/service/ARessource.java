package at.htlkrems.library.service;

import at.htlkrems.library.model.AEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;

public abstract class ARessource<A extends AEntity, ID extends Serializable> {

    private static Logger log = LoggerFactory.getLogger(ARessource.class);

    @Autowired
    private JpaRepository<A, ID> repository;

    @GetMapping(produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String ping(){
        log.info("hallo welt");

        return "hallo welt";
    }

    @GetMapping(path = "/{entityID}", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public A read(@PathVariable("entityID") ID id){
        A entity = repository.getOne(id);
        return entity;
    }

    @PostMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public A create(@RequestBody @Valid A entity){
        repository.save(entity);
        log.info("created entity " + entity.getId());

        return entity;
    }

    @PutMapping(value = "/{entityID}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@Valid A entity, @PathVariable("entityID") ID entityId){
        repository.save(entity);

        log.info("updated entity " + entityId);
    }

    @DeleteMapping("/{entityID}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("entityID") ID entityID){
        repository.deleteById(entityID);
        log.info("deleted branch " + entityID);
    }


}
