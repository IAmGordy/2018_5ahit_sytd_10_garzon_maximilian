package at.htlkrems.library.service;

import at.htlkrems.library.domain.IAuthorRepository;
import at.htlkrems.library.domain.IBookRepository;
import at.htlkrems.library.domain.ICategoryRepository;
import at.htlkrems.library.model.Author;
import at.htlkrems.library.model.Book;
import at.htlkrems.library.model.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RequestMapping("/books")
@RestController
public class BookRessource extends ARessource<Book, Long>{

    private static Logger log = LoggerFactory.getLogger(BookRessource.class);

    @Autowired
    private IBookRepository bookRepository;

    @Autowired
    private ICategoryRepository categoryRepository;


    @GetMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public Book findBookByTitle(@RequestParam("title") String title){
        log.info("title: " + title);

        return bookRepository.findBookByTitle(title);
    }

    @GetMapping(path = "/titleToken", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public BookListRepresentation findBookByTitleToken(@RequestParam("titleToken") String title){
        log.info("title: " + title);

        return BookListRepresentation.create(bookRepository.findBooksByTitleContainsIgnoreCase(title));
    }

    @GetMapping(path = "/titleTokenPaged", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public BookListRepresentation findBookByTitleTokenPaged(@RequestParam("titleToken") String title,
                                                            @RequestParam("page") Integer page,
                                                            @RequestParam("pageSize") Integer pageSize){
        log.info("title " + title);
        Page<Book> bookPage = bookRepository.findBooksByTitleContainsIgnoreCaseOrderByTitleAsc(title, new PageRequest(page, pageSize));
        return BookListRepresentation.create(bookPage.getContent());
    }

//SELBSTGEMACHTE

    @GetMapping(path = "/descriptionTokenPaged", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public BookListRepresentation findBookByDescriptionTokenPaged(@RequestParam("descriptionToken") String desc,
                                                            @RequestParam("page") Integer page,
                                                            @RequestParam("pageSize") Integer pageSize){
        log.info("description " + desc);
        Page<Book> bookPage = bookRepository.findBooksByDescriptionContainsIgnoreCaseOrderByTitleAsc(desc, new PageRequest(page, pageSize));
        return BookListRepresentation.create(bookPage.getContent());
    }

    @GetMapping(path = "/findByAuthor", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public BookListRepresentation findBooksByAuthor(@RequestParam("author") Author author){
        log.info("author " + author);
        Set<Book> bookSet = bookRepository.findBooksByAuthor(author);
        return BookListRepresentation.create(bookSet);
    }

    @GetMapping(path = "/countByAuthor", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public Long countBooksFromAuthor(@RequestParam("author") Author author){
        log.info("author " + author);
        Long counter = bookRepository.countBooksFromAuthor(author);
        return counter;
    }

    @GetMapping(path = "/findByCategory/{ID}", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public BookListRepresentation findBooksByCategory(@PathVariable("ID") Long ID){
        log.info("category " + ID);
        Set<Book> bookSet = bookRepository.findBooksbyCategory(categoryRepository.getOne(ID));
        return BookListRepresentation.create(bookSet);
    }

    @GetMapping(path = "/findByCategory", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public BookListRepresentation findBooksByCategoryOrderByTitle(@RequestParam("category") Category category,
                                                      @RequestParam("page") Integer page,
                                                      @RequestParam("pageSize") Integer pageSize){
        log.info("category " + category);
        Page<Book> bookPage = bookRepository.findBooksbyCategoryOrderByTitle(category, new PageRequest(page, pageSize));
        return BookListRepresentation.create(bookPage.getContent());
    }

    /*@DeleteMapping(path="/delete/{ID}",produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@RequestParam(value = "ID") Long ID){
        bookRepository.deleteById(ID);
        log.info("deleted branch " + ID);
    } */




}
