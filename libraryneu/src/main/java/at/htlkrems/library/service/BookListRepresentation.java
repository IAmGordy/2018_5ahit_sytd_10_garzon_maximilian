package at.htlkrems.library.service;

import at.htlkrems.library.model.Book;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@Data
public class BookListRepresentation implements Serializable {

    private Set<Book> books;

    public static BookListRepresentation create(Set<Book> books){
        return  new BookListRepresentation(books);
    }

    public static BookListRepresentation create(List<Book> books){
        return new BookListRepresentation(new HashSet<Book>(books));
    }

}
