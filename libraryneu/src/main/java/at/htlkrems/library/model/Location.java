package at.htlkrems.library.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "LOCATION_TYPE")
@Getter
@Setter
@Entity
@Table(name = "LOCATIONS")
public class Location extends AEntity{

    @Column(nullable = false, unique = true, name = "CODE")
    private String code;

}
