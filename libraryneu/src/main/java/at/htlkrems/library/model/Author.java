package at.htlkrems.library.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@Table(name = "AUTHORS")
public class Author extends Person{

    @NotNull
    @Column(name="CODE", length = 10, nullable = false)
    private String code;

    @NotNull
    @Column(length = 4000, nullable = false, name = "DESCRIPTION")
    private String description;

}
