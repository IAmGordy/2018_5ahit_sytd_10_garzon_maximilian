package at.htlkrems.library.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Embeddable
public class LibraryBookUserID implements Serializable {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private User user;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "BOOK_ID", nullable = false)
    private Book book;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "LIBRARY_ID", nullable = false)
    private Library library;

    @NotNull
    @Column(nullable = false, name = "LENDED_AT")
    private Date borrowedAt;

}
