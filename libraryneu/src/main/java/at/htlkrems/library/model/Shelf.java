package at.htlkrems.library.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@DiscriminatorValue("SHELF")
@Entity
public class Shelf extends Location{

    @OneToMany
    @JoinColumn(name = "SHELF_ID")
    private List<Book> books = new ArrayList<>();

}
