package at.htlkrems.library.model;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "LIBRARY")
public class Library extends AEntity{

    @NotNull
    @Column(unique = true, nullable = false, name = "NAME")
    private String name;

    @NotNull
    @Column(unique = true, nullable = false, name = "CODE")
    private String code;

    @Column(name = "DESCRIPTION")
    private String description;

}
