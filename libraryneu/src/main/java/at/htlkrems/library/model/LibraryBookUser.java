package at.htlkrems.library.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "LIBRARY_BOOK_USER")
public class LibraryBookUser implements Serializable {

    @EmbeddedId
    private LibraryBookUserID id;


}
