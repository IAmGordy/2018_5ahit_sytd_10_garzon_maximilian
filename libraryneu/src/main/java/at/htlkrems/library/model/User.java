package at.htlkrems.library.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@Table(name = "LIBRARY_USER")
public class User extends Person{

    @NotNull
    @Column(nullable = false, unique = true, length = 30, name = "USER_ID")
    private String userID;

    @NotNull
    @Column(nullable = false, name = "PSW")
    private String psw;

}
