package at.htlkrems.library.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("ROOMS")
public class Room extends Location{

    @OneToMany
    @JoinColumn(name = "ROOM_ID")
    private List<Shelf> shelves = new ArrayList<>();

}
