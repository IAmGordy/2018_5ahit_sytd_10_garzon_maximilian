package at.htlkrems.library.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Date;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Author.class, name = "author"),
        @JsonSubTypes.Type(value = User.class, name = "user")})
@Getter
@Setter
@Inheritance(strategy = InheritanceType.JOINED)
@Entity
@Table(name = "PERSONS")
public class Person extends AEntity{

    @NotNull
    @Length(min = 1, max = 50)
    @Column(nullable = false, length = 50, name = "GIVEN_NAME")
    private String givenName;

    @NotNull
    @Column(nullable = false, name = "LAST_NAME")
    private String lastName;

    @NotNull
    @Past
    @Temporal(TemporalType.DATE)
    @Column(nullable = false, name = "BIRTHDAY")
    private Date birthday;

}
