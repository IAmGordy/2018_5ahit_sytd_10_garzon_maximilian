package at.htlkrems.library.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "book")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@Entity
@Table(name = "BOOKS")
public class Book extends AEntity{

    @NotNull
    @Column(nullable = false, name="TITLE")
    private String title;

    @NotNull
    @Column(nullable = false, name="ISBN")
    private String isbn;

    @NotNull
    @Column(nullable = false, name="DESCRIPTION")
    private String description;

}
