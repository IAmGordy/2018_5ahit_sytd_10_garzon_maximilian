package at.htlkrems.library.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Embeddable
public class BookAuthorID implements Serializable {

    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false, name = "BOOK_ID")
    private Book book;

    @NotNull
    @ManyToOne
    @JoinColumn(nullable = false, name = "AUTHOR_ID")
    private Author author;

}
