package at.htlkrems.library.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@Entity
@Table(name = "CATEGORIES")
public class Category extends AEntity{

    @XmlElementWrapper(name = "books")
    @ManyToMany
    @JoinTable(name = "BOOK_CATEGORIES")
    private List<Book> books = new ArrayList<>();

    @XmlTransient
    @ManyToOne
    @JoinColumn(name="PARENT_CATEGORY_ID")
    private Category parentCategory;

    @XmlElement(name = "name")
    @NotNull
    @Column(nullable = false, unique = true, name = "NAME")
    private String name;

    @XmlElement(name = "description")
    @Column(name = "DESCRIPTION")
    private String description;

}
