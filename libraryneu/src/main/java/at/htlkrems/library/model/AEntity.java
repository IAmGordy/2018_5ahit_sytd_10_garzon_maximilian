package at.htlkrems.library.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Objects;

@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@MappedSuperclass
public class AEntity implements Serializable {

    @XmlAttribute(name = "id")
    @Setter
    @Getter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @XmlTransient
    @Transient
    private Long uuid;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AEntity aEntity = (AEntity) o;
        return Objects.equals(id, aEntity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String toXML() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(
                this.getClass());

        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                true);
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

        StringWriter sw = new StringWriter();
        marshaller.marshal(this, sw);

        return sw.toString();
    }

    public static <T> T toEntity(String xmlEntity, Class<T> clazz) throws JAXBException {
        JAXBContext jaxbContext =
                JAXBContext.newInstance(clazz);
        Unmarshaller unmarshaller =
                jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(xmlEntity);

        return (T)unmarshaller.unmarshal(reader);
    }


}
