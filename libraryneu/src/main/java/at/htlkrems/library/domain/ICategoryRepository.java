package at.htlkrems.library.domain;

import at.htlkrems.library.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface ICategoryRepository extends JpaRepository<Category, Long> {



}
