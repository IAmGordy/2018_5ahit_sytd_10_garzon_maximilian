package at.htlkrems.library.domain;

import at.htlkrems.library.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface IAuthorRepository extends JpaRepository<Author, Long> {
}
