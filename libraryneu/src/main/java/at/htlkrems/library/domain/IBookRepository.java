package at.htlkrems.library.domain;

import at.htlkrems.library.model.Author;
import at.htlkrems.library.model.Book;
import at.htlkrems.library.model.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public interface IBookRepository extends JpaRepository<Book, Long> {

    Book findBookByTitle(String title);

    Set<Book> findBooksByTitleContainsIgnoreCase(String title);

    Page<Book> findBooksByTitleContainsIgnoreCaseOrderByTitleAsc(String titleToken, Pageable pageable);

    Page<Book> findBooksByDescriptionContainsIgnoreCaseOrderByTitleAsc(String descriptionToken, Pageable pageable);

    @Query("select ba from BookAuthor ba where ba.id.author = :author")
    Set<Book> findBooksByAuthor(@Param("author") Author author);


    @Query("select count(ba) from BookAuthor ba where ba.id.author = :author")
    Long countBooksFromAuthor(@Param("author") Author author);

    @Query("select b from Category c join c.books b where c in (select c from Category c where c.parentCategory = :category) or c = :category")
    Set<Book> findBooksbyCategory(@Param("category") Category category);

    @Query("select b from Category c join c.books b where c in (select c from Category c where c.parentCategory = :category) or c = :category order by b.title")
    Page<Book> findBooksbyCategoryOrderByTitle(@Param("category") Category category, Pageable pageable);
}
