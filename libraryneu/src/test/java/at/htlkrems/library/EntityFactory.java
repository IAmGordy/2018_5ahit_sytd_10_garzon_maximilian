package at.htlkrems.library;

import at.htlkrems.library.model.Book;

public class EntityFactory {

    public static Book createDefaultBook(){
        Book book = new Book();

        book.setId(100l);
        book.setTitle("Twilight Imperium");
        book.setDescription("A famous SiFi Novel");
        book.setIsbn("9383820323");

        return book;
    }

}
