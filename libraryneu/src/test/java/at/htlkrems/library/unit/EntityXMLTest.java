package at.htlkrems.library.unit;

import at.htlkrems.library.domain.IBookRepository;
import at.htlkrems.library.domain.ICategoryRepository;
import at.htlkrems.library.model.AEntity;
import at.htlkrems.library.model.Book;
import at.htlkrems.library.model.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.JAXBException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class EntityXMLTest {

    private Logger log = LoggerFactory.getLogger(EntityXMLTest.class);

    @Autowired
    private IBookRepository bookRepository;

    @Autowired
    private ICategoryRepository categoryRepository;

    @Transactional
    @Test
    public void testBookXML(){
        Book book = bookRepository.getOne(1l);
        assertNotNull(book);

        String xmlRepresentation = null;
        Book transformedBook = null;

        try {
            xmlRepresentation = book.toXML();
            transformedBook = AEntity.toEntity(xmlRepresentation, Book.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        assertNotNull(xmlRepresentation);
        assertNotNull(transformedBook);

        assertEquals(book.getId(), transformedBook.getId());
        assertEquals(book.getTitle(), transformedBook.getTitle());
        assertEquals(book.getDescription(), transformedBook.getDescription());

        log.info(xmlRepresentation);
    }

    @Transactional
    @Test
    public void testCategoryXML(){
        Category category = categoryRepository.getOne(1l);
        assertNotNull(category);

        String xmlRepresentation = null;
        Category transformedEntity = null;

        try {
            xmlRepresentation = category.toXML();
            transformedEntity = AEntity.toEntity(xmlRepresentation, Category.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        assertNotNull(xmlRepresentation);
        assertNotNull(transformedEntity);

        assertEquals(category.getId(), transformedEntity.getId());
        assertEquals(category.getBooks().size(), transformedEntity.getBooks().size());

        log.info(xmlRepresentation);
    }



}
