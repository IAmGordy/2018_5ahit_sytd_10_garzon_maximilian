package at.htlkrems.library.unit;

import at.htlkrems.library.domain.IAuthorRepository;
import at.htlkrems.library.domain.IBookRepository;
import at.htlkrems.library.domain.ICategoryRepository;
import at.htlkrems.library.model.Author;
import at.htlkrems.library.model.Book;
import at.htlkrems.library.model.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class BookDomainTest {

    private static Logger log = LoggerFactory.getLogger(BookDomainTest.class);

    @Autowired
    private IBookRepository bookRepository;

    @Autowired
    private IAuthorRepository authorRepository;

    @Autowired
    private ICategoryRepository categoryRepository;

    @Test
    public void findBookByTitle(){
        Book book = bookRepository.findBookByTitle("The Wanderer");

        assertNotNull(book);
    }

    @Test
    public void findBookByTitleContainingToken(){
        Set<Book> books = bookRepository.findBooksByTitleContainsIgnoreCase("lor");
        assertEquals(3, books.size());

        Page<Book> bookPage = bookRepository.findBooksByTitleContainsIgnoreCaseOrderByTitleAsc("lor", new PageRequest(0,10));
        assertEquals(1, bookPage.getTotalPages());
        assertEquals(3, bookPage.getTotalElements());

        assertEquals(3, bookPage.getNumberOfElements());
        assertEquals(10, bookPage.getSize());

        List<Book> content = bookPage.getContent();

        content.stream().map(Book::getTitle).forEach(log::info);
    }

    @Test
    public void findBookByDescriptionToken(){
        Page<Book> bookPage = bookRepository.findBooksByDescriptionContainsIgnoreCaseOrderByTitleAsc("LORD", new PageRequest(0,10));
        assertEquals(3, bookPage.getNumberOfElements());
    }


    @Test
    public void findBookByAuthor(){
        Author author = this.authorRepository.getOne(13l);
        assertNotNull(author);

        Set<Book> books = this.bookRepository.findBooksByAuthor(author);
        assertEquals(3, books.size());
    }

    @Test
    public void countBooksByAuthor(){
        Author author = this.authorRepository.getOne(10l);
        assertNotNull(author);

        Long bookCount = this.bookRepository.countBooksFromAuthor(author);
        assertEquals(new Long(5), bookCount);
    }

    @Test
    public void findBooksByCategory(){
        Category category = categoryRepository.getOne(1L);
        assertNotNull(category);

        Set<Book> books = this.bookRepository.findBooksbyCategory(category);
        assertEquals(9, books.size());

        Category subCategory = categoryRepository.getOne(9l);
        assertNotNull(subCategory);

        books = this.bookRepository.findBooksbyCategory(subCategory);
        assertEquals(5, books.size());

        Page<Book> bookPage = this.bookRepository.findBooksbyCategoryOrderByTitle(subCategory, new PageRequest(0,10));
        assertEquals(5, bookPage.getNumberOfElements());
    }






}
