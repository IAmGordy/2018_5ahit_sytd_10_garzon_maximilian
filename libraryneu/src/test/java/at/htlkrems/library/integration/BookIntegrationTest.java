package at.htlkrems.library.integration;


import at.htlkrems.library.EntityFactory;
import at.htlkrems.library.domain.IBookRepository;
import at.htlkrems.library.model.Book;
import at.htlkrems.library.service.BookListRepresentation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@TestPropertySource("classpath:/config-test.properties")
public class BookIntegrationTest {

    private Logger log = LoggerFactory.getLogger(BookIntegrationTest.class);

    @Autowired
    private IBookRepository bookRepository;

    @Value("${test.server.url}")
    private String serverURL;

    @Value("${test.rest.context-root}")
    private String contextRoot;

    @Test
    public void pingService(){
        String requestURL = String.format("%s/%s/%s", serverURL, contextRoot, "books");
        RestTemplate restClient = new RestTemplate();

        ResponseEntity<String> httpResponse = restClient.getForEntity(requestURL, String.class);
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);

        HttpHeaders httpHeaders = httpResponse.getHeaders();

        String ping = httpResponse.getBody();
        assertEquals("hallo welt", ping);
    }

    @Test
    public void createBook(){
        String requestURL = String.format("%s/%s/%s", serverURL, contextRoot, "books");
        RestTemplate restClient = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

        Book book = EntityFactory.createDefaultBook();

        HttpEntity<Book> httpEntity = new HttpEntity<>(book, headers);
        ResponseEntity<Book> response = restClient.postForEntity(requestURL, httpEntity, Book.class);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        Book createdBook = response.getBody();

        assertNotNull(createdBook.getId());
    }


    @Test
    public void getBook(){
        String readBookByIdURL = String.format("%s/%s/%s/%s", serverURL, contextRoot, "books", 1l);
        RestTemplate restClient = new RestTemplate();

        ResponseEntity<Book> response =  restClient.getForEntity(readBookByIdURL, Book.class);

        Book retrievedBook = response.getBody();
        assertNotNull(retrievedBook);
        assertEquals(new Long(1), retrievedBook.getId());
    }

    @Test
    public void findBookByTitle(){
        String readBookByIdURL = String.format("%s/%s/%s?%s=%s", serverURL, contextRoot, "books", "title", "The Wanderer");
        RestTemplate restClient = new RestTemplate();

        ResponseEntity<Book> response =  restClient.getForEntity(readBookByIdURL, Book.class);

        Book retrievedBook = response.getBody();
        assertNotNull(retrievedBook);
        assertEquals(new Long(6), retrievedBook.getId());
    }

    @Test
    public void findBookByTitleToken(){
        String readBookByIdURL = String.format("%s/%s/%s/%s?%s=%s", serverURL, contextRoot, "books", "titleToken","titleToken", "wander");
        RestTemplate restClient = new RestTemplate();

        ResponseEntity<BookListRepresentation> response =  restClient.getForEntity(readBookByIdURL, BookListRepresentation.class);

        BookListRepresentation retrievedBooks = response.getBody();
        assertNotNull(retrievedBooks);
        assertEquals(1, retrievedBooks.getBooks().size());
    }

    @Test
    public void findBookByTitleTokenPaged(){
        String readBookByIdURL = String.format("%s/%s/%s/%s?%s=%s&%s=%s&%s=%s",
                serverURL, contextRoot, "books", "titleTokenPaged",
                "titleToken", "wander", "page", 0, "pageSize", 10);

        RestTemplate restClient = new RestTemplate();

        ResponseEntity<BookListRepresentation> response =  restClient.getForEntity(readBookByIdURL, BookListRepresentation.class);

        BookListRepresentation retrievedBooks = response.getBody();
        assertNotNull(retrievedBooks);
        assertEquals(1, retrievedBooks.getBooks().size());
    }



}
